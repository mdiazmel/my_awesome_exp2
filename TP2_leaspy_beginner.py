# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.10.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # First steps with Leaspy
#
# Welcome for the second practical session of the day!
#
# ### Objectives : 
# - Learn to use Leaspy methods
#
#
# # The set-up
#
# As before, if you have followed the [installation
# details](https://gitlab.com/icm-institute/aramislab/disease-course-mapping-solutions)
# carefully, you should 
#
# - be running this notebook in the `leaspy_tutorial` virtual environment
# - having all the needed packages already installed
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 1 💬</span> __Run the following command lines__

# +
import os
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# import main classes from Leaspy package
from leaspy import Leaspy, Data, AlgorithmSettings, IndividualParameters

# -

# # Part I: Data
#
# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> Data
# that can be used as a leaspy input should have the following format.
#
# #### This result in multiple rows per subject. The input format **_MUST_** follow the following rules:
# - A column named `ID`: corresponds to the subject indices
# - A columns named `TIME`: corresponds to the subject's age at the corresponding visit
# - One column per feature
# - Each row is a visit, therefore the concatenation of the subject ID, the
#   patient age at which the corresponding visit occured, and then the feature
#   values
#
# #### Concerning the features' values, as we are using a logistic model, they **_MUST_**:
# - Be between 0 and 1
# - In average increase with time for each subject (normal states correspond to
#   values near 0 and pathological states to values near 1)
#
# Moreover, to calibrate the progression model, we highly recommend to keep
# subjects that have been seen at least two times. You probably noticed that
# there are NaN: do not worry, Leaspy can handle them ;)
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 2 💬</span> __Run the following lines to load the data.__

data_path = os.path.join(os.getcwd(),'.', "data/")
df = pd.read_csv(data_path + 'simulated_data-corrected.csv')
df = df.set_index(["ID","TIME"])
df.head()

# <span style='color: #a13203; font-weight: 600;'>💬 Question 3 💬</span> __Does the data set seem to have the good format?__
#
# Your answer: ...
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 4 💬</span> __How many patients are there in the dataset?__

# +
# To complete

# +
# –––––––––––––––– #
# –––– Answer –––– #
# –––––––––––––––– #

n_subjects = df.index.get_level_values('ID').unique().shape[0]
print(f'{n_subjects} subjects in the dataset.')
# -

# <span style='color: #a13203; font-weight: 600;'>💬 Question 5 💬</span> __Create a training test that contains the first 160 patients and a testing set the rest. Each set will only contain the following features:__
# - __MDS1_total__
# - __MDS2_total__
# - __MDS3_off_total__
#
# Help : Be careful, one patient is not one line ...

# +
# To complete

# df_train = ######################
# df_test = ######################

# +
# –––––––––––––––– #
# –––– Answer –––– #
# –––––––––––––––– #

df_train = df.loc[:'GS-160'][["MDS1_total", "MDS2_total", "MDS3_off_total"]]
df_test = df.loc['GS-161':][["MDS1_total", "MDS2_total", "MDS3_off_total"]]
# -

# ### Leaspy's `Data` container
#
#
# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span>
# _Leaspy_ comes with its own data containers. The one used in a daily basis is
# `Data`. You can load your data from a csv with it `Data.from_csv_file` or
# from a DataFrame `Data.from_dataframe`.
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 6 💬</span> __Run
# the following lines to convert DataFrame into Data object.__

data_train = Data.from_dataframe(df_train)
data_test = Data.from_dataframe(df_test)

# # Part II : Instantiate a `Leaspy` object
#
# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> Before
# creating a leaspy object, you need to choose the type of progression shape
# you want to give to your data. The available models are the following:
# - linear 
# - logistic 
#
# with the possibility to enforce a _parallelism_ between the features.
# **_Parallelism_** imposes that all the features have the same average pace of
# progression.
#
# Once that is done, you just have to call `Leaspy('model_name')`. The
# dedicated names are  :
# - `univariate_linear`
# - `linear`
# - `univariate_logistic`
# - `logistic`
# - `logistic_parallel`
# - `lme_model`
# - `constant_model`
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 7 💬</span> __We
# choose a logistic model. Run the following line to instantiate the leaspy
# object.__

leaspy = Leaspy("logistic", source_dimension=2)

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span>
# `Leaspy` object contains all the main methods provided by the software. With
# this object, you can:
# - **calibrate** a model
# - **personalize** a model to individual data (basically you infer the random effects with a gradient descent)
# - **estimate** the features values of subjects at given ages based on your calibrated model and their individual parameters
# - **simulate** synthetic subjects base on your calibrated model, a collection of individual parameters and data
# - **load** and **save** a model
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 8 💬</span> __Check it out by running the following line__

# ? Leaspy

# <span style='color: #a13203; font-weight: 600;'>💬 Question 9 💬</span> __This `Leaspy` object comes with an handy attribute for vizualization. Let's have a look on the data that will be used to calibrate our model__

leaspy.model.dimension

ax = leaspy.plotting.patient_observations(data_train, alpha=.7, figsize=(14, 6))
ax.set_ylim(0,.8)
ax.grid()
plt.show()

# Well... not so engaging, right? Let's see what Leaspy can do for you.

# # Part III : Choose your algorithms
#
# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> Once
# you choosed your model, you need to choose an algorithm to calibrate it.
#
# To run any algorithm, you need to specify the settings of the related
# algorithm thanks to the `AlgorithmSettings` object. To ease Leaspy's usage
# for new users, we specified default values for each algorithm. Therefore, the
# name of the algorithm used is enough to run it. The one you need to fit your
# progression model is `mcmc_saem`, which stands for <a
# href="https://en.wikipedia.org/wiki/Markov_chain_Monte_Carlo"
# target="_blank">Markov chain Monte Carlo</a> - Stochastic Approximation of <a
# href="https://en.wikipedia.org/wiki/Expectation%E2%80%93maximization_algorithm"
# target="_blank">Expectation Maximization</a>. 
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 10 💬</span>
# __Run the following line to instanciate a `AlgorithmSettings` object.__

algo_settings = AlgorithmSettings('mcmc_saem', 
                                  n_iter=100,           # n_iter defines the number of iterations
                                  loss='MSE_diag_noise', # estimate the residual noise scaling per feature
                                  progress_bar=True)     # To display a nice progression bar during calibration

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> You
# can specify many more settings that are left by default for now. You can also
# save and load an `AlgorithmSettings` object in a json file.
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 11 💬</span>
# __Run the following line to get more informations.__

# ? AlgorithmSettings

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> It is
# often usefull, even if it is optional to store the different logs of the
# model during the iterations. You can use the following method with the path
# of the folder where the logs will be stored.
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 12 💬</span> __Run the following lines.__

algo_settings.set_logs(
    path='logs',          # Creates a logs file ; if existing, ask if rewrite it
    plot_periodicity=50,  # Saves the values to display in pdf every 50 iterations
    save_periodicity=10,  # Saves the values in csv files every 10 iterations
    console_print_periodicity=None,  # If = N, it display logs in the console/terminal every N iterations
    overwrite_logs_folder=True       # Default behaviour raise an error if the folder already exists.
)

# # Part IV : Fit your model
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 13 💬</span>
# __Run the following lines to fit the model.__

leaspy.fit(data_train, algorithm_settings=algo_settings)

# +
# –––––––––––––––– #
# –––– Answer –––– #
# –––––––––––––––– #

leaspy = Leaspy.load('outputs/model_parameters.json')
# -

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> This
# might take several minutes, so let's discuss about the _keyword argument_
# `source_dimension`. This parameters depend on the number of variable you want
# the model to learn: it can go from 1 to the number of variables. If it is not
# set by the user the default value is $\sqrt{N_{features}}$ as it has been
# shown empirically to give good results. You will learn more about the
# mathematical formulation of the model below (part V). 

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> Before
# assuming that the model is estimated, you have to check that the convergence
# went well. For that, you can look  the at the convergence during the
# iterations. To do so, you can explore the `logs` folder (in the same folder
# than this jupyter notebook) that shows the model convergence during the
# iterations. The first thing to look at is probably the
# `plots/convergence_1.pdf` and `plots/convergence_2.pdf` files : a run has had
# enough iterations to converge if the last 20 or even 30% of the iterations
# were stable for all the parameters. If not, you should provably re-run it
# with more iterations. 

from IPython.display import IFrame
IFrame('./logs/plots/convergence_1.pdf', width=990, height=670)

# <span style='color: #a13203; font-weight: 600;'>💬 Question 14 💬</span>
# __Check out the parameters of the model that are stored here__

leaspy.model.parameters

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span>
# Parameters are probably not straightfoward for now. The most important one is
# probably `noise_std`. It corresponds to the standard deviation of the
# Gaussian errors (one per feature). The smallest, the better - up to the lower
# bound which is the intrinsic noise in the data. Note that usually, cognitive
# measurements have an intrinsic error (computed on test-retest exams) between
# 5% and 10%.
#
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 15 💬</span>
# __Let's display `noise_std`__

# +
noise = leaspy.model.parameters['noise_std']
features = leaspy.model.features

print('Standard deviation of the residual noise for the feature:')
for n, f in zip(noise, features):
    print(f'- {f}: {n*100:.2f}%')
# -

# <span style='color: #a13203; font-weight: 600;'>💬 Question 16 💬</span> __Save the model with the command below__

leaspy.save("outputs/model_parameters.json", indent=2)

# <span style='color: #a13203; font-weight: 600;'>💬 Question 17 💬</span> __Load the model with the command below__

leaspy = Leaspy.load('outputs/model_parameters.json')

# <span style='color: #015e75; font-weight: 600;'>ℹ️ Information ℹ️</span> Now
# that we have sufficient evidence that the model has converged, let's output
# what the average progression looks like! 
#
# First, let's detail a bit what we are going to represent. We are going to
# display a trajectory: it corresponds to the temporal progression of the
# biomarkers. There is not only one trajectory for a cohort, as each subject
# has his or her own specific trajectory, meaning his or her disease
# progression. Each of these individual trajectories rely on individual
# parameters that are subject-specific. We will see those individual parameters
# a bit later, do not worry. For now, let's stick to the _average_ trajectory.
#
# So what does the average trajectory corresponds to? The average trajectory
# correspond to a _virtual patient_ whose individual parameters are the average
# individual parameters. And these averages are already estimated during the
# calibration.
#
# <span style='color: #a13203; font-weight: 600;'>💬 Question 18 💬</span>
# __Let's plot the average trajectory__

ax = leaspy.plotting.average_trajectory(alpha=1, figsize=(14,6))
ax.grid()
plt.show()
